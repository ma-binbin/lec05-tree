package BTree;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
public class Tree<T extends Comparable<T>> {
    private Node<T> root;
    private int size = 0;
    public Tree() {
        this.root = null;
    }
    public Tree(T value) {
        this.root = new Node<>(value);
        size++;
    }
    public int getSize() {
        return size;
    }
    public boolean isEmpty() {
        return getSize() == 0;
    }
    /*该方法用于查找指定元素的前驱或者后继*/
    public void frontBack(T value, BTreePsrl treePSRL) {
        if (findElement(value)) {
            switch (treePSRL) {
                case BINARY_TREE_PRECURSOR -> {
                    Node<T> parentElement = null;
                    Node<T> childElement = root;
                    while (childElement.value.compareTo(value) != 0) {
                        if (childElement.value.compareTo(value) > 0) {
                            parentElement = childElement;
                            childElement = childElement.left;
                        } else {
                            parentElement = childElement;
                            childElement = childElement.right;
                        }
                    }
                    if (parentElement == null) {
                        System.out.println("该元素没有前驱");
                    } else {
                        System.out.println("该元素的前驱是:");
                        System.out.println(parentElement.value);
                    }
                }
                case BINARY_TREE_SUBSEQUENT -> {
                    Queue<Node<T>> queue = new LinkedList<>();
                    queue.offer(root);
                    while (!queue.isEmpty() && queue.peek().value.compareTo(value) != 0) {
                        if (queue.peek().value.compareTo(value) > 0) {
                            queue.offer(queue.peek().left);
                        } else {
                            queue.offer(queue.peek().right);
                        }
                        queue.poll();
                    }
                    if (queue.peek().left != null) {
                        queue.offer(queue.peek().left);
                    }
                    if (queue.peek().right != null) {
                        queue.offer(queue.peek().right);
                    }
                    queue.poll();
                    if (queue.isEmpty()) {
                        System.out.println("该元素没有后继");
                    } else {
                        System.out.println("该元素的后继有:");
                        while (!queue.isEmpty()) {
                            System.out.print(queue.poll().value + " ");
                        }
                        System.out.println();
                    }
                }
                case BINARY_TREE_PS -> {
                    frontBack(value, BTreePsrl.BINARY_TREE_PRECURSOR);
                    frontBack(value, BTreePsrl.BINARY_TREE_SUBSEQUENT);
                }
                default -> System.out.println("查找类型错误");
            }
        } else {
            System.out.println("找不到该元素");
        }
    }
    /*该方法用于删除用户指定的元素*/
    public boolean appointDelete(T deleteElement) {//删除元素
        if (isEmpty()) {
            return false;
        } else {
            if (deleteElement.compareTo(root.value) == 0) {
                return deleteRoot();
            } else {
                return appointDelete(new Node<>(deleteElement));
            }
        }
    }
    private boolean appointDelete(Node<T> deleteElement) {
        Node<T> parentElement = null;
        Node<T> childElement = root;
        while (childElement != null) {
            if (childElement.value.compareTo(deleteElement.value) > 0) {
                parentElement = childElement;
                childElement = childElement.left;
            } else if (childElement.value.compareTo(deleteElement.value) < 0) {
                parentElement = childElement;
                childElement = childElement.right;
            } else {
                if (parentElement == null) {
                    return deleteRoot();
                } else if (parentElement.value.compareTo(childElement.value) > 0) {
                    return delete(parentElement, childElement, BTreePsrl.BINARY_TREE_LEFT);
                } else {
                    return delete(parentElement, childElement, BTreePsrl.BINARY_TREE_RIGHT);
                }
            }
        }
        return false;
    }
    /*该方法用于删除二叉树的root节点*/
    private boolean deleteRoot() {
        if (root.left == null && root.right == null) {
            root = null;
        } else if (root.left == null || root.right == null) {
            root = Objects.requireNonNullElseGet(root.left, () -> root.right);
        } else {
            root.value = findLeftMaxAndDelete(root.left);
        }
        return true;
    }
    private boolean delete(Node<T> parentElement, Node<T> childElement, BTreePsrl rightOrLeft) {
        switch (rightOrLeft) {
            case BINARY_TREE_LEFT -> {
                if (childElement.left == null && childElement.right == null) {
                    parentElement.left = null;
                } else if (childElement.left == null || childElement.right == null) {
                    parentElement.left = Objects.requireNonNullElseGet(childElement.left, () -> childElement.right);
                } else {
                    childElement.value = findLeftMaxAndDelete(childElement.left);
                }
                size--;
                return true;
            }
            case BINARY_TREE_RIGHT -> {
                if (childElement.left == null && childElement.right == null) {
                    parentElement.right = null;
                } else if (childElement.left == null || childElement.right == null) {
                    parentElement.right = Objects.requireNonNullElseGet(childElement.left, () -> childElement.right);
                } else {
                    childElement.value = findLeftMaxAndDelete(childElement.left);
                }
                size--;
                return true;
            }
            default -> {
                try {
                    throw new Exception("删除的类型错误");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        }
    }
    /*该方法用于返回deleteElement的left树中最大的值*/
    private T findLeftMaxAndDelete(Node<T> deleteElement) {
        Node<T> parentElement = deleteElement;
        Node<T> childElement = deleteElement.right;
        while (childElement != null && childElement.right != null) {
            parentElement = childElement;
            childElement = childElement.right;
        }
        parentElement.right = childElement.left;
        return childElement.value;
    }
    /*查找元素*/
    public boolean findElement(T value) {
        Node<T> findElement = root;
        while (findElement != null) {
            if (findElement.value.compareTo(value) == 0) {
                return true;
            } else if (findElement.value.compareTo(value) > 0) {
                findElement = findElement.left;
            } else {
                findElement = findElement.right;
            }
        }
        return false;
    }
    /*插入元素*/
    public boolean insertElement(T value) {
        if (isEmpty()) {
            root = new Node<>(value);
            size++;
            return true;
        } else {
            if (findElement(value)) {
                return false;
            } else {
                size++;
                return insertElement(new Node<>(value), root);
            }
        }
    }
    private boolean insertElement(Node<T> node, Node<T> now) {
        if (now.value.compareTo(node.value) > 0) {
            if (now.left != null) {
                return insertElement(node, now.left);
            } else {
                now.left = node;
                return true;
            }
        } else {
            if (now.right != null) {
                return insertElement(node, now.right);
            } else {
                now.right = node;
                return true;
            }
        }
    }
    /*前序遍历*/
    public void preOrderTraversal() {
        preOrderTraversal(root);
    }
    private void preOrderTraversal(Node<T> root) {
        if (root != null) {
            System.out.print(root.value + " ");
            preOrderTraversal(root.left);
            preOrderTraversal(root.right);
        }
    }
    /*中序遍历*/
    public void inOrderTraversal() {
        inOrderTraversal(root);
    }
    private void inOrderTraversal(Node<T> root) {
        if (root != null) {
            inOrderTraversal(root.left);
            System.out.print(root.value + " ");
            inOrderTraversal(root.right);
        }
    }
    /*后序遍历*/
    public void postOrderTraversal() {
        postOrderTraversal(root);
    }
    private void postOrderTraversal(Node<T> root) {
        if (root != null) {
            postOrderTraversal(root.left);
            postOrderTraversal(root.right);
            System.out.print(root.value + " ");
        }
    }
    public enum BTreePsrl {
        BINARY_TREE_PRECURSOR,
        BINARY_TREE_SUBSEQUENT,
        BINARY_TREE_RIGHT,
        BINARY_TREE_LEFT,
        BINARY_TREE_PS
    }
    private static class Node<T extends Comparable<T>> {
        private Node<T> left;
        private Node<T> right;
        private T value;

        private Node(T value) {
            this.left = null;
            this.right = null;
            this.value = value;
        }
    }
}