class Main {
	public static void main(String[] args){
		BinaryTree bt = new BinaryTree();

		/*
		*         10
		*     5          15
		*  3     7    12    20
		* preOrder: 10 5 3 7 15 12 20
		*/
		Node<Integer> node10 = new Node<Integer>(10);
		bt.root = node10;

		Node<Integer> node5 = new Node<Integer>(5);
		Node<Integer> node3 = new Node<Integer>(3);
		Node<Integer> node7 = new Node<Integer>(7);
		Node<Integer> node15 = new Node<Integer>(15);
		Node<Integer> node12 = new Node<Integer>(12);
		Node<Integer> node20 = new Node<Integer>(20);

		node10.left = node5;
		node10.right = node15;

		node5.left = node3;
		node5.right = node7;

		node15.left = node12;
		node15.right = node20;

		bt.preOrderTraversal();
		
}