import java.util.Queue;
import java.util.LinkedList;

public class BinaryTree {
	public Node root;

	BinaryTree(){
		root = null;
	}

	/*
	*         10
	*     5          15
	*  3     7    12    20
	* preOrder: 10 5 3 7 15 12 20
	*/
	public void preOrderTraversal(){
		preOrder(root);
	}
	public void preOrder(Node tree){

		// 1. 树为空
		// 2. 递归的终止条件
		if(tree != null ){
			// 打印当前节点
			System.out.print(tree.value);
			System.out.print(" "); 

			// 打印左边
			let temp = tree.left;
			preOrder(temp);
			// //// 打印当前
			// temp.value
			// //// 打印左边
			// temp.left.value 
			// //// 打印右边
			// temp.right.value 

			// 打印右边
			let temp2 = tree.right
			preOrder(temp2);
			// temp2.value
			// temp2.left.value
			// temp2.right.value			
		}
	}

	public void inOrder(){}
	public void postOrder(){}
	public void levelOrder(){}
}