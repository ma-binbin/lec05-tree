public class Node<T> {
	public T value;
	public Node left;
	public Node right;


	Node(T val){
		value = val;
		left = null;
		right = null;
	}	
}